
const FIRST_NAME = "Vlad";
const LAST_NAME = "Apostol";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value>Number.MAX_SAFE_INTEGER || value<Number.MIN_SAFE_INTEGER){
        return NaN;
    }
    else{
        return parseInt(value);
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

